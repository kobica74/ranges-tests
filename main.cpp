#include <fmt/format.h>

#include <range/v3/all.hpp>

namespace {
    std::vector<int> read_data() {
        return {10, 12, 30, 5, 60, 700, 8, 99, 10};
    }
}

int main() {


    // views
    {
        std::vector<int> vi{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        auto rng = vi | ranges::view::remove_if([](int i) { return i % 2 == 1; })
                   | ranges::view::transform([](int i) { return std::to_string(i); });
        auto print_vector = [](const std::string& v){ fmt::print("{0} ", v); };
        ranges::for_each(rng, print_vector);
        fmt::print("\n");
    }

    {
        std::vector<int> vi =
                ranges::view::for_each(ranges::view::ints(1,10), [](int i){
                    return ranges::yield_from(ranges::view::repeat_n(i,i));
                });
        auto print_vector = [](const int v){ fmt::print("{0} ", v); };
        ranges::for_each(vi, print_vector);
        fmt::print("\n");
    }

    // actions
    {
        std::vector<int> vi = read_data() | ranges::action::sort | ranges::action::unique;
        auto print_vector = [](const int v){ fmt::print("{0} ", v); };
        ranges::for_each(vi, print_vector);
        fmt::print("\n");
    }

    return 0;
}